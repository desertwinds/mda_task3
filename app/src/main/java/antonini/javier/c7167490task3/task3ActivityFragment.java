package antonini.javier.c7167490task3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


public class task3ActivityFragment extends Fragment {

    int total_tries = 0;
    int correct_answers = 0;
    String player_name = "";
    TextView nameView;
    TextView welcomeView;
    TextView scoreView;
    MenuItem menuGame;
    MenuItem menuList;
    final int GAME_START_CODE = 1;
    final int LIST_START_CODE = 2;

    public task3ActivityFragment() {
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_task3, container, false);
        nameView = (TextView) rootView.findViewById(R.id.player_name);
        welcomeView = (TextView) rootView.findViewById(R.id.hello_message);
        scoreView = (TextView) rootView.findViewById(R.id.player_score);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_task3, menu);
        menuGame = (MenuItem) menu.findItem(R.id.action_game);
        menuList = (MenuItem) menu.findItem(R.id.action_list);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_game) {
            Intent gameIntent = new Intent(getActivity(), task1Activity.class);
            startActivityForResult(gameIntent, GAME_START_CODE);
        }

        if (id == R.id.action_list){
            Intent listIntent = new Intent(getActivity(), task2Activity.class);
            startActivityForResult(listIntent, LIST_START_CODE);
        }

        if (id == R.id.action_setup_name){
            setupName();
        }

        if (id == R.id.action_reset_score){
            resetScore();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setupName(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Set name");

        final EditText input = new EditText(getActivity());

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                player_name = input.getText().toString();
                nameView.setText(getString(R.string.player_text) + player_name);
                manageViewWithInfo();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void resetScore(){
        total_tries = 0;
        correct_answers = 0;
        scoreView.setText(getString(R.string.score_text) + getScore());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LIST_START_CODE) {
            manageViewWithInfo();
        }
        if (requestCode == GAME_START_CODE){
            manageViewWithInfo();
        }
    }





    public void manageViewWithInfo(){
        nameView.setVisibility(View.VISIBLE);
        scoreView.setText(getString(R.string.score_text) + getScore());
        scoreView.setVisibility(View.VISIBLE);
        welcomeView.setVisibility(View.INVISIBLE);
        menuList.setVisible(true);
        menuGame.setVisible(true);
    }

    public String getScore(){
        return correct_answers + "/" + total_tries;
    }
}
