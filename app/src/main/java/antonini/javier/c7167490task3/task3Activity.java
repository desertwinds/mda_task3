package antonini.javier.c7167490task3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class task3Activity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task3);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new task3ActivityFragment())
                    .commit();
        }
    }

}
