package antonini.javier.c7167490task3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Random;

/**
 * A placeholder fragment containing a simple view.
 */
public class task1ActivityFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LOG_TAG = "TASK_FRAGMENT";
    private String champion_name;
    private String champion_attr;
    private Integer attr_value;
    private EditText userInput;
    private TextView hintText;


    public task1ActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        champion_name = getResources().getString(R.string.champion_name);
        champion_attr = getResources().getString(R.string.champion_attr);
        attr_value = 50;
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_task1, container, false);
        Button answerButton = (Button) rootView.findViewById(R.id.answerBtn);
        Button hintButton = (Button) rootView.findViewById(R.id.hintBtn);
        answerButton.setOnClickListener(answerClickListener);
        hintButton.setOnClickListener(hintClickListener);
        userInput = (EditText)rootView.findViewById(R.id.userInput);
        hintText = (TextView)rootView.findViewById(R.id.hintText);

        new getChampion(this).execute();
        return rootView;
    }

    public void setChampion(View view, String version, String championUrl){
        ImageView champion_image =  (ImageView) view.findViewById(R.id.championImg);
        TextView question = (TextView) view.findViewById(R.id.question);
        String q = "Do you know " + champion_name + "'s " + champion_attr + "?";
        question.setText(q);
        champion_image.requestLayout();
        champion_image.getLayoutParams().height = 550;
        champion_image.getLayoutParams().width = 400;
        new DownloadImageTask(champion_image).execute(version, championUrl);
    }


    private View.OnClickListener answerClickListener = new View.OnClickListener(){
        public void onClick(View view){
            String input = userInput.getEditableText().toString();
            if (input.matches("")){
                hintText.setText("Come on, make a guess! It's not that hard.");
            }
            else {
                int value = Integer.parseInt(input);
                if (value == attr_value){
                    hintText.setText("Woohoo! You are right! " + champion_name + "'s " + champion_attr + " is " + attr_value);
                }
                else{
                    int difference = Math.abs(value - attr_value);
                    if (difference > 0 && difference < value/10)
                        hintText.setText("You almost got it! A little bit closer.");
                    else if (difference >= value/10 && difference < value/4)
                        hintText.setText("Getting closer.");
                    else if (difference >= value/4 && difference < value/2)
                        hintText.setText("Keep guessing. You are as cold as Ashe's arrows.");
                    else if (difference >= value/2)
                        hintText.setText("You are extremely cold. Even Lissandra thinks you over did it.");
                }
            }

        }
    };

    private View.OnClickListener hintClickListener = new View.OnClickListener(){
        public void onClick(View view){
            int high = attr_value + (attr_value*2/3);
            int low = attr_value - (attr_value*2/3);
            hintText.setText("I can say for sure that the value we are looking for is between " + low + " and "+ high);
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragmentmenu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.refresh){
            hintText.setText("");
            new getChampion(this).execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        private String ddragon_base = "ddragon.leagueoflegends.com";
        private String ddragon_cdn = "cdn";
        private String ddragon_img = "img";
        private String ddragon_champion = "champion";

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... params) {
            String version = params[0];
            String champion = params[1];
            Bitmap mIcon11 = null;

            try {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("http")
                        .authority(ddragon_base)
                        .appendPath(ddragon_cdn)
                        .appendPath(version)
                        .appendPath(ddragon_img)
                        .appendPath(ddragon_champion)
                        .appendPath(champion);
                InputStream in = new java.net.URL(builder.build().toString()).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private class getChampion extends AsyncTask<Void, Void, JSONObject> {
        private task1ActivityFragment mFragment;
        private String base_riot_api_url = "global.api.pvp.net";
        private String riot_api_api = "api";
        private String riot_api_lol = "lol";
        private String riot_api_data = "static-data";
        private String riot_api_region = "euw";
        private String riot_api_version = "v1.2";
        private String riot_api_champion = "champion";
        private String riot_api_api_key = "2d15c384-1a7a-4231-9485-dd12be6b9520";
        private String riot_api_query_key = "api_key";
        private String riot_api_query_order = "dataById";
        private String riot_api_query_data = "champData";
        private String riot_api_order = "true";
        private String riot_api_query_get_data = "image,stats";
        private String riot_champion_version;
        private String LOG_TAG = "Get Champion";
        private String[] stats_slug = {"armor", "attackdamage", "attackrange", "hp", "movespeed", "mp"};
        private String[] stats_names = {"Armor", "Attack Damage", "Attack Range", "Health Points", "Movement Speed", "Mana Points"};

        public getChampion(task1ActivityFragment fragment){
            this.mFragment = fragment;
        }

        protected JSONObject doInBackground(Void... a){
            JSONObject result = null;
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String championsJsonStr = null;

            try{
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("https")
                        .authority(base_riot_api_url)
                        .appendPath(riot_api_api)
                        .appendPath(riot_api_lol)
                        .appendPath(riot_api_data)
                        .appendPath(riot_api_region)
                        .appendPath(riot_api_version)
                        .appendPath(riot_api_champion)
                        .appendQueryParameter(riot_api_query_order, riot_api_order)
                        .appendQueryParameter(riot_api_query_data, riot_api_query_get_data)
                        .appendQueryParameter(riot_api_query_key, riot_api_api_key);

                URL url = new URL(URLDecoder.decode(builder.build().toString(), "UTF-8"));

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                championsJsonStr = buffer.toString();
            }
            catch (IOException e){
                Log.e(LOG_TAG, "Error", e);
                return null;
            }
            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                result = new JSONObject(championsJsonStr);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
            }

            return result;
        }

        protected void onPostExecute(JSONObject championsData){
            String version = "";
            JSONObject data;
            JSONObject image;
            JSONObject stats;
            JSONArray championIds;
            String championName = "Aatrox";
            String imageUrl = "Aatrox.png";
            int championSize;
            int selectedStatPosition = 1;
            String selectedStatValue = "50";
            int championPosition;
            Random random = new Random();
            try {
                version = championsData.getString("version");
                data = championsData.getJSONObject("data");
                championIds = data.names();
                championSize = championIds.length();
                championPosition = random.nextInt(championSize);
                championName = data.getJSONObject(championIds.getString(championPosition)).getString("name");
                image = data.getJSONObject(championIds.getString(championPosition)).getJSONObject("image");
                stats = data.getJSONObject(championIds.getString(championPosition)).getJSONObject("stats");
                imageUrl = image.getString("full");
                selectedStatPosition = random.nextInt(stats_slug.length);
                selectedStatValue = stats.getString(stats_slug[selectedStatPosition]);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            mFragment.attr_value = (int) Double.parseDouble(selectedStatValue);
            mFragment.champion_attr = stats_names[selectedStatPosition];
            mFragment.champion_name = championName;

            View fragmentRoot = mFragment.getView();

            mFragment.setChampion(fragmentRoot, version, imageUrl);
        }
    }
}
