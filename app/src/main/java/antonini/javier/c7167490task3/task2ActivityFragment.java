package antonini.javier.c7167490task3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A placeholder fragment containing a simple view.
 */
public class task2ActivityFragment extends Fragment {

    private final String LOG_TAG = "Champions Fragment";
    myAdapter adapter;
    ArrayList<Champion> champions;
    String version;

    public task2ActivityFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task2, container, false);
        champions = new ArrayList<Champion>(0);

        adapter = new myAdapter(
                getActivity(),
                R.layout.listview_champion,
                champions);
        ListView championsView = (ListView) rootView.findViewById(R.id.listview_champions);
        championsView.setAdapter(adapter);
        championsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), adapter.getToast(position), Toast.LENGTH_SHORT).show();
            }
        });
        new getChampion().execute();
        return rootView;
    }

    public class myAdapter extends ArrayAdapter<Champion> {
        Context context;
        ArrayList<Champion> champions;
        int resource;

        public myAdapter(Context context, int resource, ArrayList<Champion> champions){
            super(context, resource, champions);
            this.context = context;
            this.champions = champions;
            this.resource = resource;
        }

        public String getToast(int position){
            Champion champion = champions.get(position);
            String name = champion.getName();
            String title = champion.getTitle();
            return "You have selected " + name + ", " + title;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(resource, parent, false);
            }

            Champion champion = getItem(position);

            if (champion != null){
                TextView nameTv = (TextView) v.findViewById(R.id.champion_name);
                ImageView imgTv = (ImageView) v.findViewById(R.id.champion_image);
                TextView movementTv = (TextView) v.findViewById(R.id.movementText);
                TextView armorTv = (TextView) v.findViewById(R.id.armorText);
                TextView damageTv = (TextView) v.findViewById(R.id.adText);
                TextView resistTv = (TextView) v.findViewById(R.id.magicResistText);
                if(nameTv != null){
                    nameTv.setText(champion.getName());
                }
                if (movementTv != null)
                    movementTv.setText(champion.getMS());
                if (armorTv != null)
                    armorTv.setText(champion.getArmor());
                if (damageTv != null)
                    damageTv.setText(champion.getAD());
                if (resistTv != null)
                    resistTv.setText(champion.getMR());
                if (imgTv != null)
                    new DownloadImageTask(imgTv).execute(version, champion.getImage_url());

            }

            return v;
        }

        private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
            ImageView bmImage;
            private String ddragon_base = "ddragon.leagueoflegends.com";
            private String ddragon_cdn = "cdn";
            private String ddragon_img = "img";
            private String ddragon_champion = "champion";

            public DownloadImageTask(ImageView bmImage) {
                this.bmImage = bmImage;
            }

            protected Bitmap doInBackground(String... params) {
                String version = params[0];
                String champion = params[1];
                Bitmap mIcon11 = null;

                try {
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority(ddragon_base)
                            .appendPath(ddragon_cdn)
                            .appendPath(version)
                            .appendPath(ddragon_img)
                            .appendPath(ddragon_champion)
                            .appendPath(champion);
                    InputStream in = new java.net.URL(builder.build().toString()).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                return mIcon11;
            }

            protected void onPostExecute(Bitmap result) {
                bmImage.setImageBitmap(result);
            }
        }
    }



    private class getChampion extends AsyncTask<Void, Void, JSONObject> {
        private String base_riot_api_url = "global.api.pvp.net";
        private String riot_api_api = "api";
        private String riot_api_lol = "lol";
        private String riot_api_data = "static-data";
        private String riot_api_region = "euw";
        private String riot_api_version = "v1.2";
        private String riot_api_champion = "champion";
        private String riot_api_api_key = "2d15c384-1a7a-4231-9485-dd12be6b9520";
        private String riot_api_query_key = "api_key";
        private String riot_api_query_order = "dataById";
        private String riot_api_query_data = "champData";
        private String riot_api_order = "true";
        private String riot_api_query_get_data = "image,stats";
        private String riot_champion_version;
        private String LOG_TAG = "Get Champion";
        private String[] stats_slug = {"armor", "attackdamage", "attackrange", "hp", "movespeed", "mp"};
        private String[] stats_names = {"Armor", "Attack Damage", "Attack Range", "Health Points", "Movement Speed", "Mana Points"};

        public getChampion(){
        }

        protected JSONObject doInBackground(Void... a){
            JSONObject result = null;
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String championsJsonStr = null;

            try{
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("https")
                        .authority(base_riot_api_url)
                        .appendPath(riot_api_api)
                        .appendPath(riot_api_lol)
                        .appendPath(riot_api_data)
                        .appendPath(riot_api_region)
                        .appendPath(riot_api_version)
                        .appendPath(riot_api_champion)
                        .appendQueryParameter(riot_api_query_order, riot_api_order)
                        .appendQueryParameter(riot_api_query_data, riot_api_query_get_data)
                        .appendQueryParameter(riot_api_query_key, riot_api_api_key);

                URL url = new URL(URLDecoder.decode(builder.build().toString(), "UTF-8"));

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                championsJsonStr = buffer.toString();
            }
            catch (IOException e){
                Log.e(LOG_TAG, "Error", e);
                return null;
            }
            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                result = new JSONObject(championsJsonStr);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
            }

            return result;
        }

        protected void onPostExecute(JSONObject championsData){
            JSONObject data;
            JSONObject stats;
            ArrayList<Champion> list = new ArrayList<Champion>();
            Champion champion;
            Iterator<String> iterator;
            String name_attr;
            JSONObject img_attr;
            String img_string;
            String armor_attr;
            String ad_attr;
            String mr_attr;
            String ms_attr;
            String title_attr;
            try {
                if (championsData != null){
                    version = championsData.getString("version");
                    data = championsData.getJSONObject("data");
                    iterator = data.keys();
                    while (iterator.hasNext()){
                        String key = iterator.next();
                        JSONObject champ = data.getJSONObject(key);
                        name_attr = champ.getString("name");
                        img_attr = champ.getJSONObject("image");
                        img_string = img_attr.getString("full");
                        stats = champ.getJSONObject("stats");
                        armor_attr = stats.getString("armor");
                        armor_attr = Integer.toString((int) Double.parseDouble(armor_attr));
                        ad_attr = stats.getString("attackdamage");
                        ad_attr = Integer.toString((int) Double.parseDouble(ad_attr));
                        mr_attr = stats.getString("spellblock");
                        mr_attr = Integer.toString((int) Double.parseDouble(mr_attr));
                        ms_attr = stats.getString("movespeed");
                        ms_attr = Integer.toString((int) Double.parseDouble(ms_attr));
                        title_attr = champ.getString("title");
                        champion = new Champion(name_attr, img_string, armor_attr, mr_attr, ms_attr, ad_attr, title_attr);
                        list.add(champion);
                    }
                    adapter.clear();
                    //Log.v(LOG_TAG, list.get(20).getName());
                    adapter.addAll(list);
                    //adapter.notifyDataSetChanged();
                }

                /*championName = data.getJSONObject(championIds.getString(championPosition)).getString("name");
                image = data.getJSONObject(championIds.getString(championPosition)).getJSONObject("image");
                stats = data.getJSONObject(championIds.getString(championPosition)).getJSONObject("stats");
                imageUrl = image.getString("full");
                selectedStatPosition = random.nextInt(stats_slug.length);
                selectedStatValue = stats.getString(stats_slug[selectedStatPosition]);
                */
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }




    public class Champion {
        String name;
        String image_url;
        String armor;
        String magic_resist;
        String movement;
        String attack_damage;
        String title;

        public Champion(String name, String image_url, String armor, String mr, String ms, String ad, String title){
            this.name = name;
            this.image_url = image_url;
            this.armor = armor;
            this.magic_resist = mr;
            this. movement = ms;
            this.attack_damage = ad;
            this.title = title;
        }

        public String getName(){
            return this.name;
        }

        public String getArmor() { return (this.armor);}

        public String getMR() { return(this.magic_resist);}

        public String getMS() { return (this.movement);}

        public String getAD() { return (this.attack_damage);}

        public String getImage_url() { return this.image_url;}

        public String getTitle() { return this.title; }
    }
}
